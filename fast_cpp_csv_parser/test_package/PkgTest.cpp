/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <csv.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Fast C++ CSV Parser' package test (compilation, linking, and execution).\n";

    // just test header include

    std::cout << "'Fast C++ CSV Parser' package works!" << std::endl;
    return EXIT_SUCCESS;
}
