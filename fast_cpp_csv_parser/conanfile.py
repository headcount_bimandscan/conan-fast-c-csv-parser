#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class FastCppCSVParser(ConanFile):
    name = "fast_cpp_csv_parser"
    version = "20180930"
    license = "BSD-3-Clause"
    url = "https://bitbucket.org/headcount_bimandscan/conan-fast-c-csv-parser"
    description = "A small, easy-to-use, and fast header-only library for reading comma-separated value (CSV) files."
    generators = "txt"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://github.com/ben-strasser/fast-cpp-csv-parser"
    no_copy_source = True

    _src_dir = f"_{name}_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    exports = "../LICENCE.md"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++11 or higher!")

        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        git_uri = "https://github.com/ben-strasser/fast-cpp-csv-parser.git"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "master")
        git.checkout(element = "5a3443e05b5f2993c903ae2881a95390265bb54b") # commit hash -> 30/9/2018

    def build(self):
        pass

    def package(self):
        self.copy("csv.h",
                  "include",
                  self._src_dir)

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

    def package_id(self):
        self.info.header_only()
