# BIM & Scan® Third-Party Library (Fast C++ CSV Parser)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for Ben Strasser's [Fast C++ CSV Parser](https://github.com/ben-strasser/fast-cpp-csv-parser), a small, easy-to-use, and fast header-only library for reading comma-separated value (CSV) files.

Requires a C++11 compiler (or higher).

Supports snapshot 30/9/2018 (unstable, from Git repo).
