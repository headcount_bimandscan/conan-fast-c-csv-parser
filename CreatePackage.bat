@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'README.md' in the project root for more information.
REM

CALL conan create --build="missing" %* "fast_cpp_csv_parser" "bimandscan/unstable"

@ECHO ON
