@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'README.md' in the project root for more information.
REM

CALL conan upload -c -r "bimandscan-public" --all "fast_cpp_csv_parser/20180930@bimandscan/unstable"

@ECHO ON
